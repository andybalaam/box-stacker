
// Create an observeDOM method.
// Credit: https://stackoverflow.com/questions/3219758/detect-changes-in-the-dom
var observeDOM = (function(){
  var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

  return function( obj, callback ){
    if( !obj ) return;

    if( MutationObserver ){
      // define a new observer
      var mutationObserver = new MutationObserver(callback)

      // have the observer observe for changes in children
      mutationObserver.observe( obj, { childList:true, subtree:true })
      return mutationObserver
    }

    // browser support fallback
    else if( window.addEventListener ){
      obj.addEventListener('DOMNodeInserted', callback, false)
      obj.addEventListener('DOMNodeRemoved', callback, false)
    }
  }
})()



let post_bodies = null;
const processed_post_bodies = [];

const re_play = /box-stacker.artificialworlds.net\/game\/#play=(\d+)/;
const re_soln = /box-stacker.artificialworlds.net\/game\/#solution=(\d+)/;

function process_post_body(post_body) {
    const html = post_body.innerHTML;
    const m_play = html.match(re_play);
    if (m_play) {
        const level_id = parseInt(m_play[1], 10);
        const iframe = document.createElement("iframe");
        iframe.setAttribute("src", `/game/#play=${level_id}`);
        iframe.style.width = "480px";
        iframe.style.height = "480px";
        iframe.style.border = "none";
        iframe.style.backgroundColor = "#CCC";
        post_body.appendChild(iframe);
    }

    const m_soln = html.match(re_soln);
    if (m_soln) {
        const level_id = parseInt(m_soln[1], 10);
        const button = document.createElement("button");
        button.style.width = "480px";
        button.innerText = "Click to reveal";
        button.onclick = () => {
            const iframe = document.createElement("iframe");
            iframe.setAttribute("src", `/game/#solution=${level_id}`);
            iframe.style.width = "480px";
            iframe.style.height = "480px";
            iframe.style.border = "none";
            iframe.style.backgroundColor = "#CCC";
            post_body.removeChild(button);
            post_body.appendChild(iframe);
        };
        post_body.appendChild(button);
    }
}

function check_for_new_posts() {
    if (post_bodies === null) {
        // This is a live collection, so it updates whenever something changes.
        post_bodies = document.getElementsByClassName("Post-body");
    }

    for (const post_body of post_bodies) {
        if (!processed_post_bodies.includes(post_body)) {
            process_post_body(post_body);
            processed_post_bodies.push(post_body);
        }
    }
}

observeDOM(document, check_for_new_posts);
