# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.5.9] - 2023-08-14

### Changed

- Clarify the text of links to uploaded levels

## [1.5.8] - 2023-06-22

### Changed

- Allow changing all the URLs we use to store levels, post to the forum etc.

## [1.5.7] - 2023-06-16

### Added

- Link to the web site and privacy policy from the menu on the title screen.

## [1.5.6] - 2023-06-15

### Fixed

- Ensure the version check really finds the latest version by adding a
  cachebuster to the url.

## [1.5.5] - 2023-06-14

### Fixed

- Prevent dropping an object if your finger slips while clicking a button

## [1.5.4] - 2023-06-11

### Added

- Check for new versions of the app and display and upgrade button

### Fixed

- Prevent block-duplication #2

### Changed

- Address license issues to facilitate F-Droid inclusion

## [1.5.3] - 2023-06-05

### Fixed

- Really hide smolpxl bar when there isn't space

## [1.5.2] - 2023-06-05

### Added

- Lots of new levels from the community forums, including some Extremes!

### Fixed

- Drag and drop should now work on desktop and mobile
- Click to place should also work on desktop and mobile
- Hide smolpxl bar when there isn't space

## [1.5.1] - 2023-05-08

### Changed

- Add a smolpxl bar and anonymous play stats
- Different layout to help with mobile packaging

## [1.5.0] - 2023-02-24

### Changed

- Allow drag-and-drop from buttons

## [1.4.2] - 2023-02-13

### Fixed

- Fix 'level.filename undefined'

## [1.4.1] - 2023-02-13

### Fixed

- Store the canonical names of levels you completed, so we remember even if the
  order is shuffled around.

## [1.4.0] - 2023-02-13

### Changed

- Move "Show solution" button to avoid accidentally clicking it
- Split levels into easy, medium and hard sections

## [1.3.1] - 2023-02-04

### Fixed

- Fix bug causing 'TypeError: level.get is not a function'

## [1.3.0] - 2023-02-04

### Changed

- Re-jig the first five levels to make a clearer intro to how to play
- Replace Mountain Climb with Mountain Climber, which is a tiny bit harder
- Add some gifs that show how to play

## [1.2.2] - 2023-02-01

### Changed

- Pre-fetch all levels at the beginning, but without blocking the game from
  starting.

## [1.2.1] - 2023-02-01

### Changed

- Load levels when they are needed, not all at once at the beginning

## [1.2.0] - 2023-01-19

### Changed

- Count only the blocks you added, not the ones included in the level

## [1.1.2] - 2023-01-14

### Fixed

- Prevent buttons being squashed on iOS

## [1.1.1] - 2023-01-08

### Fixed

- Set the zoom level when you click Play in the level editor

## [1.1.0] - 2023-01-08

### Added

- "Focus" mode that removes almost all UI buttons
- Hover help on icon buttons

### Changed

- Remember the zoom position when editing levels

### Fixed

- Allow manually zooming out as far as some levels are set up for
- Improve auto-zoom of solution views

## [1.0.3] - 2023-01-05

### Fixed

- Prevent crash when there are no shapes in a level
- Fix: Crazy Golf solution uses blocks you are not allowed!
- Fix: In Up the instructions say "car" when they should say "ball"

### Changed

- Remember previous camera zoom and location when you switch between modes

## [1.0.2] - 2022-12-22

### Fixed

- Fixed a bug meaning solutions just show "Loading..." forever

## [1.0.1] - 2022-12-21

### Added

- Added version number to front page
- Added link to forum on front page

### Changed

- Changed timings to be more consistent

### Fixed

- Fixed solution viewer to display the full solution

## [1.0.0] - 2022-12-19

### Added

- The Game
