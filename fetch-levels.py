#!/usr/bin/env python3

import json
import os
import re
import requests
import sys

output_dir = "public/levels/04_extreme"
first_level_num = 1
file_url = "https://scores.artificialworlds.net/api/v1/box-stacker/levels/file/"
whitespace_re = re.compile("\\s+")
nonchar_re = re.compile("[^a-zA-Z0-9]")

if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

def fetch_level(id):
    return json.loads(requests.get("%s%s" % (file_url, id)).json()["contents"])

class Level:
    def __init__(self, ln, i):
        spl = whitespace_re.split(ln)
        if len(spl) < 3:
            raise Exception("Line %d '%s' is not valid." % (i + 1, ln.strip()))
        self.difficulty = spl[0]
        self.main_id = spl[1]
        self.solution_id = spl[2]
        self.main_json = None
        self.solution_json = None

    def fetch(self):
        self.main_json = fetch_level(self.main_id)
        self.solution_json = fetch_level(self.solution_id)

def write_level(i, lev):
    lev.main_json["name"] = lev.main_json["name"].replace(" (fixed)", "")
    lev.solution_json["name"] = lev.main_json["name"]
    slug = nonchar_re.sub("_", lev.main_json["name"]).lower()

    full_name = "%s/%02d_%s" % (output_dir, i + first_level_num, slug)
    print(full_name)

    with open("%s.json" % full_name, "w") as f:
        f.write(json.dumps(lev.main_json, indent = 4))
    with open("%s_solution.json" % full_name, "w") as f:
        f.write(json.dumps(lev.solution_json, indent = 4))

for (i, ln) in enumerate(filter(lambda ln: ln.strip() != "", sys.stdin)):
    lev = Level(ln, i)
    lev.fetch()
    write_level(i, lev)
